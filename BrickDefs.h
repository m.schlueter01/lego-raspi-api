#pragma once

enum class Output{
	A = 1,
	B = 2,
	C = 4,
	D = 8
};

enum class Input{
	Port1 = 0,
	Port2 = 1,
	Port3 = 2,
	Port4 = 3,
	PortA = 16,
	PortB = 17,
	PortC = 18,
	PortD = 19
};